﻿namespace FitnessApp.ViewModel
{
    using System;
    using System.Threading.Tasks;
    using Emgu.CV;
    using System.Drawing;
    using FitnessApp.Common.MVVM;
    using FitnessApp.Logic;
    using FitnessApp.Model;
    using System.IO;
    using System.Text;
    using System.Collections.Generic;
    using System.Windows.Media.Imaging;
    using Emgu.CV.Structure;
    using System.Windows.Media;

    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IDatabaseController databaseController;
        private static Random random = new Random();
        private VideoCapture capture = new VideoCapture();

        // Tab control
        private int selectedTabIndex;

        // Page visibility controls
        private bool loginPage = true;
        private bool mainPage = false;
        private bool detailsPage = false;
        private bool newPassPage = false;
        private bool addPassPage = false;

        // User who works in the gym
        private string userName;
        private string password;
        private Felhasznalok felhasznalo;

        // Client (adding/updating)
        private string name;
        private string email;
        private string phone;
        private string cnp;
        private string address;
        private string notes;
        private byte[] image;

        // Client list
        private string clientSearchField;
        private List<Kliensek> clients;

        // Details page
        private Kliensek selectedClient;
        private List<ClientTickets> clientTickets;
        private KliensBerletei selectedClientPass;
        private bool editButton;

        // Passes page
        private List<BerletTipusok> gymPasses;
        private List<FTermek> gyms;
        private FTermek selectedGym;

        // NewPasses page
        private string passName;
        private string price;
        private int daysValid;
        private int entries;
        private int maxDailyUsage;
        private int from;
        private int to;

        // Entries page
        private List<Belepesek> clientEntries;

        // Add pass to client page
        private BerletTipusok selectedGymPass;



        public MainWindowViewModel(IDatabaseController databaseController)
        {
            this.databaseController = databaseController;
            Instance = this;

            this.LoginCommand = new RelayCommand(this.LoginExecute);
            this.SearchClientsCommand = new RelayCommand(this.SearchClientsExecute);
            this.TakePictureCommand = new RelayCommand(this.TakePictureExecute);
            this.AddClientCommand = new RelayCommand(this.AddClientExecute);
            this.OpenClientDetailsCommand = new RelayCommand(this.OpenClientDetailsExecute);
            this.ResumeToMainPageCommand = new RelayCommand(this.ResumeToMainPageExecute);
            this.OpenNewPassPageCommand = new RelayCommand(this.OpenNewPassPageExecute);
            this.AddNewPassCommand = new RelayCommand(this.AddNewPassExecute);
            this.DeleteClientCommand = new RelayCommand(this.DeleteClientExecute);
            this.EntryCommand = new RelayCommand(this.EntryExecute);
            this.AddPassToClientCommand = new RelayCommand(this.AddPassToClientExecute);
            this.AddPassClientCommand = new RelayCommand(this.AddPassClientExecute);
        }

        public static MainWindowViewModel Instance { get; private set; }

        // PROPERTIES
        public string UserName
        {
            get
            {
                return this.userName;
            }

            set
            {
                if (!string.Equals(this.userName, value))
                {
                    this.userName = value;
                    this.RaisePropertyChanged();
                }
            }
        }
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                if (!string.Equals(this.password, value))
                {
                    this.password = value;
                    this.RaisePropertyChanged();
                }
            }
        }
        public bool LoginPage
        {
            get
            {
                return this.loginPage;
            }

            set
            {
                this.loginPage = value;
                this.RaisePropertyChanged();
            }
        }
        public bool MainPage
        {
            get
            {
                return this.mainPage;
            }

            set
            {
                this.mainPage = value;
                this.RaisePropertyChanged();
            }
        }
        public bool DetailsPage
        {
            get
            {
                return this.detailsPage;
            }

            set
            {
                this.detailsPage = value;
                RaisePropertyChanged();
            }
        }
        public bool NewPassPage 
        {
            get
            {
                return this.newPassPage;
            } 
            
            set
            {
                this.newPassPage = value;
                this.RaisePropertyChanged();
            }
        }
        public bool AddPassPage
        {
            get
            {
                return this.addPassPage;
            }

            set
            {
                this.addPassPage = value;
                this.RaisePropertyChanged();
            }
        }

        public Felhasznalok Felhasznalo
        {
            get
            {
                return this.felhasznalo;
            }

            set
            {
                this.felhasznalo = value;
                this.RaisePropertyChanged();
            }
        }
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (!string.Equals(this.name, value))
                {
                    this.name = value;
                    if (this.selectedGym != null && this.selectedGym.nev != "All")
                    {
                        GymPasses = databaseController.getBerletByGymNameAsync(this.selectedGym.nev).Result;

                        if (name != null)
                            ClientEntries = databaseController.getBelepesekByNamesAsync(this.selectedGym.nev, name).Result;
                        else
                            ClientEntries = databaseController.getBelepesekByNamesAsync(this.selectedGym.nev).Result;
                    }
                    else
                    {
                        GymPasses = databaseController.getBerletAsync().Result;

                        if (name != null)
                            ClientEntries = databaseController.getBelepesekByNamesAsync(null, name).Result;
                        else
                            ClientEntries = databaseController.getBelepesekAsync().Result;
                    }
                    this.RaisePropertyChanged();
                }
            }
        }
        public string Email
        {
            get
            {
                return this.email;
            }

            set
            {
                if (!string.Equals(this.email, value))
                {
                    this.email = value;
                    this.RaisePropertyChanged();
                }
            }
        }
        public string Phone
        {
            get
            {
                return this.phone;
            }

            set
            {
                if (!string.Equals(this.phone, value))
                {
                    this.phone = value;
                    this.RaisePropertyChanged();
                }
            }
        }
        public string Address
        {
            get
            {
                return this.address;
            }

            set
            {
                if (!string.Equals(this.address, value))
                {
                    this.address = value;
                    this.RaisePropertyChanged();
                }
            }
        }
        public string CNP
        {
            get
            {
                return this.cnp;
            }

            set
            {
                if (!string.Equals(this.cnp, value))
                {
                    this.cnp = value;
                    this.RaisePropertyChanged();
                }
            }
        }
        public string Notes
        {
            get
            {
                return this.notes;
            }

            set
            {
                if (!string.Equals(this.notes, value))
                {
                    this.notes = value;
                    this.RaisePropertyChanged();
                }
            }
        }
        public byte[] Image
        {
            get
            {
                return this.image;
            }

            set
            {
                this.image = value;
                RaisePropertyChanged();
            }
        }
        public List<Kliensek> Clients
        {
            get
            {
                return this.clients;
            }

            set
            {
                this.clients = value;
                RaisePropertyChanged();
            }
        }
        public string ClientSearchField
        {
            get
            {
                return this.clientSearchField;
            }

            set
            {
                this.clientSearchField = value;
                RaisePropertyChanged();
            }
        }
        public Kliensek SelectedClient
        {
            get
            {
                return this.selectedClient;
            }

            set
            {
                this.selectedClient = value;
                RaisePropertyChanged();
            }
        }
        public int SelectedTabIndex
        {
            get
            {
                return this.selectedTabIndex;
            }

            set
            {
                this.selectedTabIndex = value;
                this.RaisePropertyChanged();
            }
        }
        public List<ClientTickets> ClientTickets 
        { 
            get
            {
                return this.clientTickets;
            }
            
            set
            {
                this.clientTickets = value;
                this.RaisePropertyChanged();
            }
        }
        public List<BerletTipusok> GymPasses
        { 
            get
            {
                return this.gymPasses;
            }

            set
            {
                this.gymPasses = value;
                this.RaisePropertyChanged();
            }
        }
        public List<FTermek> Gyms
        {
            get
            {
                return this.gyms;
            }

            set
            {
                this.gyms = value;
                this.RaisePropertyChanged();
            }
        }
        public FTermek SelectedGym
        {
            get
            {
                return this.selectedGym;
            }

            set
            {
                this.selectedGym = value;
                if(this.selectedGym != null && this.selectedGym.nev != "All")
                {
                    GymPasses = databaseController.getBerletByGymNameAsync(this.selectedGym.nev).Result;

                    if(name != null)
                        ClientEntries = databaseController.getBelepesekByNamesAsync(this.selectedGym.nev, name).Result;
                    else
                        ClientEntries = databaseController.getBelepesekByNamesAsync(this.selectedGym.nev).Result;
                }
                else
                {
                    GymPasses = databaseController.getBerletAsync().Result;

                    if (name != null)
                        ClientEntries = databaseController.getBelepesekByNamesAsync(name).Result;
                    else
                        ClientEntries = databaseController.getBelepesekAsync().Result;
                }
                this.RaisePropertyChanged();
            }
        }
        public string PassName 
        {
            get
            {
                return this.passName;
            }

            set
            {
                this.passName = value;
                this.RaisePropertyChanged();
            }
        }
        public string Price
        {
            get
            {
                return this.price;
            }

            set
            {
                this.price = value;
                this.RaisePropertyChanged();
            }
        }
        public int DaysValid
        {
            get
            {
                return this.daysValid;
            }

            set
            {
                this.daysValid = value;
                this.RaisePropertyChanged();
            }
        }
        public int Entries
        {
            get
            {
                return this.entries;
            }

            set
            {
                this.entries = value;
                this.RaisePropertyChanged();
            }
        }
        public int MaxDailyUsage
        {
            get
            {
                return this.maxDailyUsage;
            }

            set
            {
                this.maxDailyUsage = value;
                this.RaisePropertyChanged();
            }
        }
        public int From
        {
            get
            {
                return this.from;
            }

            set
            {
                this.from = value;
                this.RaisePropertyChanged();
            }
        }
        public int To
        {
            get
            {
                return this.to;
            }

            set
            {
                this.to = value;
                this.RaisePropertyChanged();
            }
        }
        public List<Belepesek> ClientEntries
        {
            get
            {
                return this.clientEntries;
            }

            set
            {
                this.clientEntries = value;
                this.RaisePropertyChanged();
            }
        }
        public BerletTipusok SelectedGymPass
        {
            get
            {
                return this.selectedGymPass;
            }

            set
            {
                this.selectedGymPass = value;
                this.RaisePropertyChanged();
            }
        }
        public KliensBerletei SelectedClientPass
        {
            get
            {
                return this.selectedClientPass;
            }

            set
            {
                this.selectedClientPass = value;
                this.RaisePropertyChanged();
            }
        }
        public bool EditButton
        {
            get
            {
                return this.editButton;
            }

            set
            {
                this.editButton = value;
                if(editButton == false)
                {
                    /*selectedClient.nev = name;
                    selectedClient.email = email;
                    selectedClient.telefon = phone;
                    selectedClient.cim = address;
                    selectedClient.szemelyi = cnp;
                    selectedClient.Megjegyzesek = notes;*/

                    databaseController.updateKliensAsync(selectedClient);
                }
                this.RaisePropertyChanged();
            }
        }

        // COMMANDS

        public RelayCommand LoginCommand
        {
            get;
        }
        private void LoginExecute()
        {
            this.Felhasznalo = databaseController.getFelhasznaloAsync(this.userName, this.password).Result;

            if (this.felhasznalo != null)
            {
                // Load all clients
                this.Clients = databaseController.getKliensAsync(null, 0).Result;
                this.Gyms = databaseController.getFteremAsync().Result;
                var g = new FTermek();
                g.nev = "All";
                this.gyms.Insert(0, g);
                this.Gyms = this.gyms;
                this.GymPasses = databaseController.getBerletAsync().Result;
                ClientEntries = databaseController.getBelepesekAsync().Result;

                this.LoginPage = false;
                this.MainPage = true;
            }
        }
        public RelayCommand SearchClientsCommand
        {
            get;
        }
        private void SearchClientsExecute()
        {
            this.Clients = databaseController.getKliensAsync(clientSearchField).Result;
        }
        public RelayCommand TakePictureCommand
        {
            get;
        }
        private void TakePictureExecute()
        {
            if (capture == null)
            {
                capture = new VideoCapture();
            }

            capture.ImageGrabbed += this.ImageGrabbed;
            capture.Start();
        }
        public RelayCommand AddClientCommand
        {
            get;
        }
        private void AddClientExecute()
        {
            if (name != null && email != null && phone != null && address != null && cnp != null)
            {
                Kliensek k = new Kliensek();

                int id = databaseController.GetLastId(new Kliensek());

                k.Id = id + 1;
                k.nev = name;
                k.email = email;
                k.telefon = phone;
                k.cim = address;
                k.szemelyi = cnp;
                k.Megjegyzesek = notes;
                k.inserted_date = DateTime.Now;
                k.is_deleted = false;
                k.photo = image;
                k.vonalkod = RandomDigits(13);

                databaseController.addKliensAsync(k);

                this.Name = null;
                this.Email = null;
                this.Phone = null;
                this.CNP = null;
                this.Address = null;
                this.Notes = null;
                this.Image = null;

                this.Clients = databaseController.getKliensAsync(null, 0).Result;

                this.SelectedTabIndex = 0;
            } 

        }   
        public RelayCommand OpenClientDetailsCommand
        {
            get;
        }
        private void OpenClientDetailsExecute()
        {
            this.MainPage = false;
            this.DetailsPage = true;

            var ct = databaseController.getKliensBerletByIdAsync(selectedClient.Id).Result;

            List<ClientTickets> ctl = new List<ClientTickets>();
            foreach(KliensBerletei c in ct)
            {
                ClientTickets tmp = new ClientTickets();

                tmp.Berlet = c.Berlet;
                tmp.Id = c.Id;
                tmp.kliens = c.kliens;
                tmp.terem = c.terem;
                tmp.vasarlasi_datum = c.vasarlasi_datum;
                tmp.Ervenyes = c.Ervenyes;
                tmp.ElsoHasznalat_datum = c.ElsoHasznalat_datum;
                tmp.eladasiar = c.eladasiar;
                tmp.EddigiBelepesszam = c.EddigiBelepesszam;
                tmp.vonalkod = c.vonalkod;


                tmp.EntriesLeft = c.Berlet.hanybelepesreervenyes - c.EddigiBelepesszam;
                tmp.DaysValabil = c.Berlet.hanynapigervenyes - DateTime.Now.Subtract(c.vasarlasi_datum).Days;
                tmp.TimeLimit = c.Berlet.Hanyoratol.ToString() + "-" + c.Berlet.Hanyoraig;

                ctl.Add(tmp);
            }

            ClientTickets = ctl;
        }
        public RelayCommand ResumeToMainPageCommand
        {
            get;
        }
        private void ResumeToMainPageExecute()
        {
            this.Clients = databaseController.getKliensAsync(null, 0).Result;
            this.MainPage = true;
            this.DetailsPage = false;
        }
        public RelayCommand OpenNewPassPageCommand
        {
            get;
        }
        private void OpenNewPassPageExecute()
        {
            this.MainPage = false;
            this.NewPassPage = true;
        }
        public RelayCommand AddNewPassCommand
        {
            get;
        }
        private void AddNewPassExecute()
        {
            if(passName != null && price != null)
            {
                var pass = new BerletTipusok();

                var id = databaseController.GetLastId(new BerletTipusok());

                pass.Id = id + 1;
                pass.megnevezes = passName;
                pass.ar = price;
                pass.hanynapigervenyes = daysValid;
                pass.hanybelepesreervenyes = entries;
                pass.Hanyoratol = from;
                pass.Hanyoraig = to;
                pass.Napontahanyszorhasznalhato = maxDailyUsage;
                pass.terem = selectedGym;
                pass.torolve = false;

                databaseController.addBerletAsync(pass);

                PassName = null;
                Price = null;
                DaysValid = 0;
                From = 0;
                To = 0;
                MaxDailyUsage = 0;
                Entries = 0;
                SelectedGym = null;

                this.GymPasses = databaseController.getBerletAsync().Result;

                this.NewPassPage = false;
                this.MainPage = true;
            }
        }
        public RelayCommand DeleteClientCommand
        {
            get;
        }
        private void DeleteClientExecute()
        {
            databaseController.removeKliensAsync(selectedClient);
            this.Clients = databaseController.getKliensAsync(null, 0).Result;
            this.DetailsPage = false;
            this.MainPage = true;
        }
        public RelayCommand EntryCommand
        {
            get;
        }
        private void EntryExecute()
        {
            var b = new Belepesek();

            var id = databaseController.GetLastId(new Belepesek());
            b.Id = id + 1;
            b.terem = selectedClientPass.terem;
            b.kliens = selectedClient;
            b.insertedby_uid = felhasznalo.Id;
            b.datum = DateTime.Now;
            b.berlet = selectedClientPass.Berlet;

            databaseController.addBelepesAsync(b);
        }
        public RelayCommand AddPassToClientCommand
        {
            get;
        }
        private void AddPassToClientExecute()
        {
            this.DetailsPage = false;
            this.AddPassPage = true;
        }
        public RelayCommand AddPassClientCommand
        {
            get;
        }
        private void AddPassClientExecute()
        {
            var id = databaseController.GetLastId(new KliensBerletei());
            var k = new KliensBerletei();
            k.Id = id+1;
            k.terem = selectedGymPass.terem;
            k.Berlet = selectedGymPass;
            k.EddigiBelepesszam = 0;
            k.eladasiar = selectedGymPass.ar;
            k.Ervenyes = true;
            k.kliens = selectedClient;
            k.vasarlasi_datum = DateTime.Now;
            k.vonalkod = RandomDigits(10);

            databaseController.addKliensBerletAsync(k);

            var kb = databaseController.getKliensBerletByIdAsync(selectedClient.Id).Result;

            List<ClientTickets> ctl = new List<ClientTickets>();
            foreach (KliensBerletei c in kb)
            {
                ClientTickets tmp = new ClientTickets();

                tmp.Berlet = c.Berlet;
                tmp.Id = c.Id;
                tmp.kliens = c.kliens;
                tmp.terem = c.terem;
                tmp.vasarlasi_datum = c.vasarlasi_datum;
                tmp.Ervenyes = c.Ervenyes;
                tmp.ElsoHasznalat_datum = c.ElsoHasznalat_datum;
                tmp.eladasiar = c.eladasiar;
                tmp.EddigiBelepesszam = c.EddigiBelepesszam;
                tmp.vonalkod = c.vonalkod;


                tmp.EntriesLeft = c.Berlet.hanybelepesreervenyes - c.EddigiBelepesszam;
                tmp.DaysValabil = c.Berlet.hanynapigervenyes - DateTime.Now.Subtract(c.vasarlasi_datum).Days;
                tmp.TimeLimit = c.Berlet.Hanyoratol.ToString() + "-" + c.Berlet.Hanyoraig;

                ctl.Add(tmp);
            }

            ClientTickets = ctl;

            ClientEntries = databaseController.getBelepesekAsync().Result;
            this.AddPassPage = false;
            this.DetailsPage = true;
        }

        // UTILS

        public string RandomDigits(int length)
        {
            string s = string.Empty;
            for (int i = 0; i < length; i++)
                s = String.Concat(s, random.Next(10).ToString());
            return s;
        }
        private void ImageGrabbed(object sender, EventArgs e)
        {
            try
            {
                Mat m = new Mat();
                capture.Retrieve(m);
                Image<Bgr, byte> img = m.ToImage<Bgr, byte>();

                this.Image = img.Bytes;
                MemoryStream stream = new MemoryStream(img.Bytes);
                //this.Image = System.Drawing.Image.FromStream(stream);
                var width = img.Width; // for example
                var height = img.Height; // for example
                var dpiX = 96d;
                var dpiY = 96d;
                var pixelFormat = PixelFormats.Pbgra32; // for example
                var bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8;
                var stride = bytesPerPixel * width;

                //this.Image = (BitmapImage)BitmapSource.Create(width, height, dpiX, dpiY,
                //                                 pixelFormat, null, img.Bytes, stride);
                capture.Stop();
            }
            catch(Exception)
            {

            }
        }
    }
}
