﻿namespace FitnessApp.Logic
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FitnessApp.Model;

    public interface IDatabaseController
    {
        Task Initialize();

        public Task addBelepesAsync(Belepesek belepes);

        public Task<List<Belepesek>> getBelepesekAsync();

        public Task addKliensAsync(Kliensek kliens);

        public Task removeKliensAsync(Kliensek kliens);

        public Task<List<Kliensek>> getKliensAsync(string nev = null, int torolve = -1);

        public Task updateKliensAsync(Kliensek kliens);

        public Task addFelhasznaloAsync(Felhasznalok felhasznalo);

        public Task<Felhasznalok> getFelhasznaloAsync(string felhasznalonev = null, string jelszo = null);

        public Task addBerletAsync(BerletTipusok berlet);

        public Task removeBerletAsync(int id);


        public Task updateBerletAsync(BerletTipusok berlet);

        public Task<List<BerletTipusok>> getBerletAsync(string nev = null);

        public Task<List<Belepesek>> getBelepesekByNamesAsync(string gymName = null, string userName = null);

        public Task<List<BerletTipusok>> getBerletByGymNameAsync(string nev = null);

        public Task addKliensBerletAsync(KliensBerletei berlet);

        public Task removeKliensBerletAsync(int id);

        public Task updateKliensBerletAsync(KliensBerletei berlet);

        public Task<List<KliensBerletei>> getKliensBerletAsync(int ervenyes = -1, string kliensNev = null);

        public Task<List<KliensBerletei>> getKliensBerletByIdAsync(int id);

        public Task addFteremAsync(FTermek fterem);

        public Task<List<FTermek>> getFteremAsync(string nev = null);

        public int GetLastId(object table);

    }
}