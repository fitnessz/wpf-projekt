﻿namespace FitnessApp.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FitnessApp.DAL.Mongo;
    using FitnessApp.Model;

    public class DatabaseController : IDatabaseController
    {
        private readonly IDBContext dbContext;

        public DatabaseController(IDBContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Task addBelepesAsync(Belepesek belepes)
        {
            return this.dbContext.addBelepesAsync(belepes);
        }

        public Task<List<Belepesek>> getBelepesekAsync()
        {
            return this.dbContext.getBelepesekAsync();
        }
        public Task<List<Belepesek>> getBelepesekByNamesAsync(string gymName = null, string userName = null) 
        {
            return this.dbContext.getBelepesekByNamesAsync(gymName, userName);
        }
        public Task addKliensAsync(Kliensek kliens)
        {
            return this.dbContext.addKliensAsync(kliens);
        }

        public Task updateKliensAsync(Kliensek kliens) {
            return this.dbContext.updateKliensAsync(kliens);
        }

        public Task removeKliensAsync(Kliensek kliens)
        {
            return this.dbContext.removeKliensAsync(kliens);
        }

        public Task<List<Kliensek>> getKliensAsync(string nev = null, int torolve = -1)
        {
            return this.dbContext.getKliensAsync(nev, torolve);
        }

        public Task addFelhasznaloAsync(Felhasznalok felhasznalo)
        {
            return this.dbContext.addFelhasznaloAsync(felhasznalo);
        }

        public Task<Felhasznalok> getFelhasznaloAsync(string felhasznalonev = null, string jelszo = null)
        {
            return this.dbContext.getFelhasznaloAsync(felhasznalonev, jelszo);
        }

        public Task addBerletAsync(BerletTipusok berlet)
        {
            return this.dbContext.addBerletAsync(berlet);
        }


        public Task removeBerletAsync(int id)
        {
            return this.dbContext.removeBerletAsync(id);
        }


        public Task updateBerletAsync(BerletTipusok berlet)
        {
            return this.dbContext.updateBerletAsync(berlet);
        }


        public Task<List<BerletTipusok>> getBerletAsync(string nev = null)
        {
            return this.dbContext.getBerletAsync(nev);
        }

        public Task<List<BerletTipusok>> getBerletByGymNameAsync(string nev = null) 
        {
            return this.dbContext.getBerletByGymNameAsync(nev);
        }

        public Task addKliensBerletAsync(KliensBerletei berlet)
        {
            return this.dbContext.addKliensBerletAsync(berlet);
        }


        public Task removeKliensBerletAsync(int id)
        {
            return this.dbContext.removeKliensBerletAsync(id);
        }


        public Task updateKliensBerletAsync(KliensBerletei berlet)
        {
            return this.dbContext.updateKliensBerletAsync(berlet);
        }


        public Task<List<KliensBerletei>> getKliensBerletAsync(int ervenyes = -1, string kliensNev = null)
        {
            return this.dbContext.getKliensBerletAsync(ervenyes, kliensNev);
        }

        public Task<List<KliensBerletei>> getKliensBerletByIdAsync(int id) {
            return this.dbContext.getKliensBerletByIdAsync(id);
        }

        public Task addFteremAsync(FTermek fterem)
        {
            return this.dbContext.addFteremAsync(fterem);
        }


        public Task<List<FTermek>> getFteremAsync(string nev = null)
        {
            return this.dbContext.getFteremAsync(nev);
        }

        public int GetLastId(object table)
        {
            return this.dbContext.GetLastId(table);
        }
        public Task Initialize()
        {
            /*Felhasznalok f = new Felhasznalok();
            f.Id = 0;
            f.felhasznalonev = "pista";
            f.jelszo = "asd";
            f.tipus = 0;
            dbContext.addFelhasznaloAsync(f);*/

            DatabaseInit init = new DatabaseInit(dbContext);

            //init.addDummyFtermek(4);
            //init.addDummyBerletTipusok(6);

            return Task.CompletedTask;
        }
    }
}
