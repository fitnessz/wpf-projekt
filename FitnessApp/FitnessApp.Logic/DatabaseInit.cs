﻿using FitnessApp.DAL.Mongo;
using FitnessApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitnessApp.Logic
{
    public class DatabaseInit
    {
        private static Random random = new Random();

        private IDBContext dbContext;

        public DatabaseInit(IDBContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public void addDummyFtermek(int db)
        {
            var rand = new Random();
            for (int i = 0; i < db; i++)
            {
                FTermek fterem = new FTermek();

                fterem.Id = i;
                fterem.nev = RandomString(5);
                fterem.is_deleted = rand.NextDouble() >= 0.5;
                dbContext.addFteremAsync(fterem);
            }
        }

        public void addDummyBerletTipusok(int db)
        {
            for (int i = 0; i < db; i++)
            {
                BerletTipusok berletTipus = new BerletTipusok();
                var hanyoratol = random.Next(0, 23);
                berletTipus.Id = i;
                berletTipus.megnevezes = RandomString(7);
                berletTipus.ar = random.Next(50, 150).ToString();
                berletTipus.hanynapigervenyes = random.Next(60);
                berletTipus.hanybelepesreervenyes = random.Next(50);
                berletTipus.torolve = random.NextDouble() >= 0.5;
                berletTipus.terem = getRandomTerem();
                berletTipus.Hanyoratol = hanyoratol;
                berletTipus.Hanyoraig = random.Next(hanyoratol, 24);
                berletTipus.Napontahanyszorhasznalhato = random.Next(10);


                dbContext.addBerletAsync(berletTipus);
            }
        }

        public FTermek getRandomTerem()
        {
            List<FTermek> terem = dbContext.getFteremAsync().Result;
            int index = random.Next(terem.Count);
            return terem[index];
        }
    }
}
