﻿using System.Collections.Generic;
using MongoDB.Driver;
using System.Threading.Tasks;
using System;
using FitnessApp.DAL.Mongo.Settings;
using FitnessApp.Model;
using MongoDB.Bson;

namespace FitnessApp.DAL.Mongo
{
    public class DBContext : IDBContext
    {
        private readonly IMongoDatabase db;

        public DBContext(IMongoDBSettings mongoDbSettings)
        {
            MongoClientSettings settings = MongoClientSettings.FromUrl(new MongoUrl(mongoDbSettings.ConnectionString));
            Client = new MongoClient(settings);
            db = Client.GetDatabase(mongoDbSettings.DatabaseName);

            KliensekCollection = db.GetCollection<Kliensek>(MongoDBSettings.KliensekCollectionName);
            BelepesekCollection = db.GetCollection<Belepesek>(MongoDBSettings.BelepesekCollectionName);
            FelhasznalokCollection = db.GetCollection<Felhasznalok>(MongoDBSettings.FelhasznalokCollectionName);
            BerletekCollection = db.GetCollection<BerletTipusok>(MongoDBSettings.BerletekCollectionName);
            FTermekCollection = db.GetCollection<FTermek>(MongoDBSettings.FTermekCollectionName);
            KliensBerleteiCollection = db.GetCollection<KliensBerletei>(MongoDBSettings.KliensBerleteiCollectionName);
        }

        public MongoClient Client { get; }

        public IMongoCollection<Kliensek> KliensekCollection { get; }

        public IMongoCollection<Belepesek> BelepesekCollection { get; }

        public IMongoCollection<Felhasznalok> FelhasznalokCollection { get; }

        public IMongoCollection<BerletTipusok> BerletekCollection { get; }

        public IMongoCollection<FTermek> FTermekCollection { get; }

        public IMongoCollection<KliensBerletei> KliensBerleteiCollection { get; }

        public int GetLastId(object table)
        {
            var asd = table.GetType().ToString();
            switch (table.GetType().ToString())
            {
                case "FitnessApp.Model.Kliensek":
                    ProjectionDefinition<Kliensek> projection = Builders<Kliensek>.Projection
                    .Include(x => x.Id);
                    return KliensekCollection.Find(new BsonDocument()).Project<Kliensek>(projection).SortBy(bson => bson.Id).FirstOrDefaultAsync().Id;

                case "FitnessApp.Model.KliensBerletei":
                    ProjectionDefinition<KliensBerletei> projection2 = Builders<KliensBerletei>.Projection
                    .Include(x => x.Id);
                    return KliensBerleteiCollection.Find(new BsonDocument()).Project<KliensBerletei>(projection2).SortBy(bson => bson.Id).FirstOrDefaultAsync().Id;

                case "FitnessApp.Model.FTermek":
                    ProjectionDefinition<FTermek> projection3 = Builders<FTermek>.Projection
                    .Include(x => x.Id);
                    return FTermekCollection.Find(new BsonDocument()).Project<FTermek>(projection3).SortBy(bson => bson.Id).FirstOrDefaultAsync().Id;

                case "FitnessApp.Model.Felhasznalok":
                    ProjectionDefinition<Felhasznalok> projection4 = Builders<Felhasznalok>.Projection
                    .Include(x => x.Id);
                    return FelhasznalokCollection.Find(new BsonDocument()).Project<Felhasznalok>(projection4).SortBy(bson => bson.Id).FirstOrDefaultAsync().Id;

                case "FitnessApp.Model.BerletTipusok":
                    ProjectionDefinition<BerletTipusok> projection5 = Builders<BerletTipusok>.Projection
                    .Include(x => x.Id);
                    return BerletekCollection.Find(new BsonDocument()).Project<BerletTipusok>(projection5).SortBy(bson => bson.Id).FirstOrDefaultAsync().Id;

                case "FitnessApp.Model.Belepesek":
                    ProjectionDefinition<Belepesek> projection6 = Builders<Belepesek>.Projection
                    .Include(x => x.Id);
                    return BelepesekCollection.Find(new BsonDocument()).Project<Kliensek>(projection6).SortBy(bson => bson.Id).FirstOrDefaultAsync().Id;
            }
            return -1;
        }
        public async Task addBelepesAsync(Belepesek belepes)
        {
            await this.BelepesekCollection.InsertOneAsync(belepes);
        }
        public Task<List<Belepesek>> getBelepesekAsync()
        {
            var filter = new BsonDocument();

            return this.BelepesekCollection.Find(filter).ToListAsync();
        }
        public Task<List<Belepesek>> getBelepesekByNamesAsync(string gymName = null, string userName = null)
        {
            FilterDefinition<Belepesek> filter;
            if (gymName != null)
            {
                if (userName != null)
                {
                    var filter1 = Builders<Belepesek>.Filter.Eq(x => x.terem.nev, gymName);
                    var filter2 = Builders<Belepesek>.Filter.Eq(x => x.kliens.nev, userName);
                    filter = Builders<Belepesek>.Filter.And(filter1, filter2);
                }
                else {
                    filter = Builders<Belepesek>.Filter.Eq(x => x.terem.nev, gymName);
                }
            }
            else {
                if (userName != null)
                {
                    filter = Builders<Belepesek>.Filter.Eq(x => x.kliens.nev, userName);
                }
                else
                {
                    filter = new BsonDocument();
                } 
            }

            return this.BelepesekCollection.Find(filter).ToListAsync();
        }
        public async Task addKliensAsync(Kliensek kliens)
        {
            await this.KliensekCollection.InsertOneAsync(kliens);
        }
        public async Task removeKliensAsync(Kliensek kliens)
        {
            var updateDef = Builders<Kliensek>.Update.Set(o => o.is_deleted, true);
            await this.KliensekCollection.UpdateOneAsync(a => a.Id == kliens.Id, updateDef);
        }
        public Task<List<Kliensek>> getKliensAsync(string nev = null, int torolve = -1)
        {
            FilterDefinition<Kliensek> filter;
            if (nev != null && nev != "")
            {
                if (torolve != -1)
                {
                    var filter1 = Builders<Kliensek>.Filter.Eq(x => x.nev, nev);
                    var filter2 = Builders<Kliensek>.Filter.Eq(x => x.is_deleted, Convert.ToBoolean(torolve));
                    filter = Builders<Kliensek>.Filter.Or(filter1, filter2);
                }
                else {
                    filter = Builders<Kliensek>.Filter.Eq(x => x.nev, nev);
                }
            }
            else
            {
                if (torolve != -1)
                {
                    filter = Builders<Kliensek>.Filter.Eq(x => x.is_deleted, Convert.ToBoolean(torolve));
                }
                else
                {
                    filter = new BsonDocument();
                }           
            }

            return this.KliensekCollection.Find(filter).ToListAsync();
        }

        public async Task updateKliensAsync(Kliensek kliens)
        {
            var filter = Builders<Kliensek>.Filter.Eq(x => x.Id, kliens.Id);
            await this.KliensekCollection.ReplaceOneAsync(filter, kliens);
        }
        public async Task addFelhasznaloAsync(Felhasznalok felhasznalo)
        {
            await this.FelhasznalokCollection.InsertOneAsync(felhasznalo);
        }
        public Task<Felhasznalok> getFelhasznaloAsync(string felhasznalonev = null, string jelszo = null)
        {
            FilterDefinition<Felhasznalok> filter;
            filter = Builders<Felhasznalok>.Filter.Eq(x => x.felhasznalonev, felhasznalonev) &
                    Builders<Felhasznalok>.Filter.Eq(x => x.jelszo, jelszo);

            return this.FelhasznalokCollection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task addBerletAsync(BerletTipusok berlet)
        {
            await this.BerletekCollection.InsertOneAsync(berlet);
        }
        public async Task removeBerletAsync(int id)
        {
            await this.BerletekCollection.DeleteOneAsync(a => a.Id == id);
        }
        public async Task updateBerletAsync(BerletTipusok berlet)
        {
            var filter = Builders<BerletTipusok>.Filter.Eq(x => x.Id, berlet.Id);
            await this.BerletekCollection.ReplaceOneAsync(filter, berlet);
        }
        public Task<List<BerletTipusok>> getBerletAsync(string nev = null)
        {
            FilterDefinition<BerletTipusok> filter;
            if (nev != null)
            {
                filter = Builders<BerletTipusok>.Filter.Eq(x => x.megnevezes, nev);
            }
            else
            {
                filter = new BsonDocument();
            }

            return this.BerletekCollection.Find(filter).ToListAsync();
        }
        public Task<List<BerletTipusok>> getBerletByGymNameAsync(string nev = null)
        {
            FilterDefinition<BerletTipusok> filter;
            if (nev != null)
            {
                filter = Builders<BerletTipusok>.Filter.Eq(x => x.terem.nev, nev);
            }
            else
            {
                filter = new BsonDocument();
            }

            return this.BerletekCollection.Find(filter).ToListAsync();
        }
        public async Task addKliensBerletAsync(KliensBerletei berlet)
        {
            await this.KliensBerleteiCollection.InsertOneAsync(berlet);
        }
        public async Task removeKliensBerletAsync(int id)
        {
            await this.KliensBerleteiCollection.DeleteOneAsync(a => a.Id == id);
        }
        public async Task updateKliensBerletAsync(KliensBerletei berlet)
        {
            var filter = Builders<KliensBerletei>.Filter.Eq(x => x.Id, berlet.Id);
            await this.KliensBerleteiCollection.ReplaceOneAsync(filter, berlet);
        }
        public Task<List<KliensBerletei>> getKliensBerletAsync(int ervenyes = -1, string kliensNev = null)
        {
            FilterDefinition<KliensBerletei> filter;
            if (ervenyes != -1)
            {
                if (kliensNev != null)
                {
                    var filterNev = Builders<KliensBerletei>.Filter.Eq(x => x.kliens.nev, kliensNev);
                    var filterErvenyes = Builders<KliensBerletei>.Filter.Eq(x => x.Ervenyes, Convert.ToBoolean(ervenyes));
                    filter = Builders<KliensBerletei>.Filter.And(filterNev, filterErvenyes);
                }
                else
                {
                    filter = Builders<KliensBerletei>.Filter.Eq(x => x.Ervenyes, Convert.ToBoolean(ervenyes));
                }
            }
            else
            {
                if (kliensNev != null)
                {
                    filter = Builders<KliensBerletei>.Filter.Eq(x => x.kliens.nev, kliensNev);
                }
                else
                {
                    filter = new BsonDocument();             
                }
            }

            return this.KliensBerleteiCollection.Find(filter).ToListAsync();
        }
        public Task<List<KliensBerletei>> getKliensBerletByIdAsync(int id)
        {
            var filter = Builders<KliensBerletei>.Filter.Eq(x =>x.kliens.Id, id);
            return this.KliensBerleteiCollection.Find(filter).ToListAsync();
        }
        public async Task addFteremAsync(FTermek fterem)
        {
            await this.FTermekCollection.InsertOneAsync(fterem);
        }
        public Task<List<FTermek>> getFteremAsync(string nev = null)
        {
            return this.FTermekCollection.Find(new BsonDocument()).ToListAsync();
        }
    }
}
