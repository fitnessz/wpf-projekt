﻿namespace FitnessApp.DAL.Mongo.Settings
{
    public class MongoDBSettings : IMongoDBSettings
    {
        public const string KliensekCollectionName = "Kliensek";

        public const string BelepesekCollectionName = "Belepesek";

        public const string FelhasznalokCollectionName = "Felhasznalok";

        public const string BerletekCollectionName= "Berletek";

        public const string FTermekCollectionName = "FTermek";

        public const string SequencesCollectionName = "Sequences";

        public const string KliensBerleteiCollectionName = "KliensBerletei";

        public string ConnectionString { get; set; }

        public string DatabaseName { get; set; }
    }
}
