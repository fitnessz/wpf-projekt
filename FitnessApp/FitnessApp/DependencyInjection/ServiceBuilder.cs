﻿namespace FitnessApp.DependencyInjection
{
  using System;
  using System.IO;
  using Microsoft.Extensions.Configuration;
  using Microsoft.Extensions.DependencyInjection;
  using Microsoft.Extensions.Options;
  using FitnessApp.DAL.Mongo;
  using FitnessApp.DAL.Mongo.Settings;
  using FitnessApp.Logic;
  using FitnessApp.View;
  using FitnessApp.ViewModel;

  public static class ServiceBuilder
  {
    public static IServiceProvider Build()
    {
      var services = new ServiceCollection();

      IConfigurationRoot configuration = services.RegisterConfigurations();

      services.RegisterDBContext(configuration);

      services.RegisterControllers();

      services.RegisterViewsAndViewModels();

      return services.BuildServiceProvider();
    }

    private static void RegisterViewsAndViewModels(this ServiceCollection services)
    {
      services.AddSingleton<MainWindowViewModel>();
      services.AddTransient<MainWindow>();
    }

    private static IConfigurationRoot RegisterConfigurations(this ServiceCollection services)
    {
      IConfigurationRoot configuration = new ConfigurationBuilder()
     .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
     .AddJsonFile("appsettings.json", false)
     .Build();
      services.AddSingleton<IConfiguration>(configuration);

      return configuration;
    }

    private static void RegisterDBContext(this ServiceCollection services, IConfigurationRoot configuration)
    {
      services.Configure<MongoDBSettings>(configuration.GetSection("MongoDBSettings"));
      services.AddSingleton<IMongoDBSettings>(sp => sp.GetRequiredService<IOptions<MongoDBSettings>>().Value);
      services.AddSingleton<IDBContext, DBContext>();
    }

    private static void RegisterControllers(this ServiceCollection services)
    {
      services.AddSingleton<IDatabaseController, DatabaseController>();
    }
  }
}
