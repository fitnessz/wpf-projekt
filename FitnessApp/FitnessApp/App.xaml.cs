﻿namespace FitnessApp
{
    using System;
    using System.Windows;
    using Microsoft.Extensions.DependencyInjection;
    using FitnessApp.DependencyInjection;
    using FitnessApp.Logic;
    using FitnessApp.View;

    public partial class App : Application
    {
        public static IServiceProvider ServiceProvider { get; private set; }

        protected override async void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            App.ServiceProvider = ServiceBuilder.Build();

            this.Resources.Add("Locator", new ViewModelLocator());

            await App.ServiceProvider.GetService<IDatabaseController>().Initialize();

            var window = App.ServiceProvider.GetService<MainWindow>();
            window.Show();
        }
    }
}