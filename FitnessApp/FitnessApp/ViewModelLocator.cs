﻿using Microsoft.Extensions.DependencyInjection;
using FitnessApp.ViewModel;

namespace FitnessApp
{
  public class ViewModelLocator
  {
    public MainWindowViewModel MainWindowViewModel => App.ServiceProvider.GetService<MainWindowViewModel>();
  }
}
