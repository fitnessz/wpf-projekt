﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FitnessApp.Model
{
    public class ClientTickets : KliensBerletei
    {

        public int DaysValabil { get; set; }

        public int EntriesLeft { get; set; }

        public string TimeLimit { get; set; }
    }
}
