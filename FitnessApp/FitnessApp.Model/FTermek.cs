﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace FitnessApp.Model
{
    public class FTermek
    {
        [BsonId]
        public int Id { get; set; }

        public string nev { get; set; }

        public Boolean is_deleted { get; set; }

    }
}
