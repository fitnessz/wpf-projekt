﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace FitnessApp.Model
{
    public class BerletTipusok
    {
        [BsonId]
        public int Id { get; set; }

        public string megnevezes { get; set; }

        public string ar { get; set; }

        public int hanynapigervenyes { get; set; }

        public int hanybelepesreervenyes { get; set; }

        public Boolean torolve { get; set; }

        public FTermek terem { get; set; }

        public int Hanyoratol { get; set; }

        public int Hanyoraig { get; set; }

        public int Napontahanyszorhasznalhato { get; set; }

    }
}
