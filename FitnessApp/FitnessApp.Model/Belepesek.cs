﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace FitnessApp.Model
{
    public class Belepesek
    {
        [BsonId]
        public int Id { get; set; }

        public Kliensek kliens { get; set; }

        public BerletTipusok berlet { get; set; }

        public DateTime datum { get; set; }

        public int insertedby_uid { get; set; }

        public FTermek terem { get; set; }

    }
}
