﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace FitnessApp.Model
{
    public class KliensBerletei
    {
        [BsonId]
        public int Id { get; set; }

        public Kliensek kliens { get; set; }

        public BerletTipusok Berlet { get; set; }

        public DateTime vasarlasi_datum { get; set; }

        public string vonalkod { get; set; }

        public int EddigiBelepesszam { get; set; }

        public string eladasiar { get; set; }

        public Boolean Ervenyes { get; set; }

        public DateTime ElsoHasznalat_datum { get; set; }

        public FTermek terem { get; set; }

    }
}
