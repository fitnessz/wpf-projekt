﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace FitnessApp.Model
{
    public class Felhasznalok
    {
        [BsonId]
        public int Id { get; set; }

        public string felhasznalonev { get; set; }

        public string jelszo { get; set; }

        public int tipus { get; set; }
    }
}
