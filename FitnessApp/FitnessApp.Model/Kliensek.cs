﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace FitnessApp.Model
{
    public class Kliensek
    {
        [BsonId]
        public int Id { get; set; }

        public string nev { get; set; }

        public string telefon { get; set; }

        public string email { get; set; }

        public Boolean is_deleted { get; set; }

        public byte[] photo { get; set; }

        public DateTime inserted_date { get; set; }

        public string szemelyi { get; set; }

        public string cim { get; set; }

        public string vonalkod { get; set; }

        public string Megjegyzesek { get; set; }
    }
}
